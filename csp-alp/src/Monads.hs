{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use <$>" #-}

module Monads where

import AST ( Defexp(..), Defs )
import Global
    ( initialEnv,
      Conf(modo),
      DeclState,
      GlEnv(cantDecl, inter, lfile, decls, trace, glb),
      Mode )

import Errors ( Error(..) )
import Control.Monad.State
    ( StateT(runStateT), MonadIO(..), MonadState(get), gets, modify )
import Control.Monad.Except
    ( ExceptT, MonadIO(..), MonadError(catchError), runExceptT )
import Control.Monad.Reader
    ( ReaderT(runReaderT), MonadReader, asks )
import System.IO ( stderr, hPrint )
import System.Random (getStdRandom, randomR)
import Data.Char (isDigit, digitToInt)

-- Mónada CSP
class (MonadIO m, MonadState GlEnv m, MonadError Error m, MonadReader Conf m) => MonadCSP m where

getMode :: MonadCSP m => m Mode
getMode = asks modo

setInter :: MonadCSP m => Bool -> m ()
setInter b = modify (\s-> s {inter = b})

getInter :: MonadCSP m => m Bool
getInter = gets inter

getDeclState :: MonadCSP m => m [DeclState]
getDeclState = gets glb

getDecls :: MonadCSP m => m [Defexp]
getDecls = gets decls

getNDecls :: MonadCSP m => m Int
getNDecls = gets cantDecl

setDecls :: MonadCSP m => Defs -> m ()
setDecls ds = modify (\s -> s { decls = ds})

printCSP :: MonadCSP m => String -> m ()
printCSP = liftIO . putStrLn

setLastFile :: MonadCSP m => FilePath -> m ()
setLastFile filename = modify (\s -> s {lfile = filename , cantDecl = 0})

getLastFile :: MonadCSP m => m FilePath
getLastFile = gets lfile

-- agrega al final para ser consistente con el orden de evaluacion
addDecl :: MonadCSP m => Defexp -> [String] -> m ()
addDecl d alph = modify (\s -> s { glb = glb s ++ [(d, alph)],
                              cantDecl = cantDecl s + 1 })

removeDecl :: MonadCSP m => m ()
removeDecl = modify (\s -> s { glb = tail $ glb s,
                               cantDecl = cantDecl s - 1 })

addDecls :: MonadCSP m => Defs -> [[String]] -> [String] -> m ()
addDecls ds as [] = return ()
addDecls ds as es =
  let decls = zip ds as
      state = filter (\(Decl s _, _) -> s `elem` es) decls
  in modify (\s -> s {
    glb = state,
    decls = ds,
    cantDecl = length ds})

addEvent :: MonadCSP m => String -> m ()
addEvent e = do
  printCSP e
  modify (\s -> s { trace = e : trace s })

eraseLastFileDecls :: MonadCSP m => m ()
eraseLastFileDecls = do
  s <- get
  let n = cantDecl s
      (_,rem) = splitAt n (glb s)
  modify (\s -> s {glb = rem, cantDecl = 0})

lookupDecl :: MonadCSP m => String -> m (Maybe Defexp)
lookupDecl nm = do
  s <- getDecls
  case filter (hasName nm) s of
    (d:_) -> return (Just d)
    [] -> return Nothing
    where hasName nm' (Decl s _) = s == nm'

choose :: MonadCSP m => m Bool
choose = do
  answer <- getStdRandom (randomR (0,1)) :: MonadCSP m => m Integer
  return $ answer == 0

getChoice :: String -> Maybe Int
getChoice [] = Nothing
getChoice (c:_) =
  if isDigit c then Just (digitToInt c) else Just 3

askevent :: MonadCSP m => String -> String -> m (Maybe Bool)
askevent e f = do
  printCSP "Elija el próximo evento a ejecutar"
  printCSP $ "1) " ++ e
  printCSP $ "2) " ++ f
  printCSP ""
  l <- liftIO getLine
  let choice = getChoice l
  return $ case choice of
    Just 1 -> Just True
    Just 2 -> Just False
    _ -> Nothing

catchErrors  :: MonadCSP m => m a -> m (Maybe a)
catchErrors c = catchError (Just <$> c)
                           (\e -> liftIO $ hPrint stderr e
                              >> return Nothing)

-- | El tipo @CSP@ es un sinónimo de tipo para una mónada construida usando dos transformadores de mónada sobre la mónada @IO@.
-- El transformador de mónad @ExcepT Error@ agrega a la mónada IO la posibilidad de manejar errores de tipo 'Errors.Error'.
-- El transformador de mónadas @StateT GlEnv@ agrega la mónada @ExcepT Error IO@ la posibilidad de manejar un estado de tipo 'Global.GlEnv'.
type CSP = ReaderT Conf (StateT GlEnv (ExceptT Error IO))

-- | Esta es una instancia vacía, ya que 'MonadCSP' no tiene funciones miembro.
instance MonadCSP CSP

runCSP' :: CSP a -> Conf -> IO (Either Error (a, GlEnv))
runCSP' c conf =  runExceptT $ runStateT (runReaderT c conf) initialEnv

runCSP:: CSP a -> Conf -> IO (Either Error a)
runCSP c conf = fmap fst <$> runCSP' c conf
