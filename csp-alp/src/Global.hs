module Global where

import AST (Defexp)

data GlEnv = GlEnv {
  inter :: Bool,        -- ^ True, si estamos en modo interactivo.
  lfile :: String,      -- ^ Último archivo cargado.
  cantDecl :: Int,      -- ^ Cantidad de declaraciones desde la última carga
  decls :: [Defexp],    -- ^ Declaraciones globales
  glb :: [DeclState],   -- ^ Entorno con declaraciones globales (*)
  trace :: [String]     -- ^ Traza de ejecucion
}

type DeclState = (Defexp, [String])
-- (*) Toda declaración tiene un alfabeto (átomos que la comprenden)

-- Tipo para representar las banderas disponibles en línea de comando.
data Mode =
    Interactive
  | Eval

newtype Conf = Conf {
    modo :: Mode
}

-- Valor del estado inicial
initialEnv :: GlEnv
initialEnv = GlEnv False "" 0 [] [] []
