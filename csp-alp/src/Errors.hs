module Errors where

import Text.Parsec.Error ( ParseError )

newtype Error =
    ParseErr ParseError

instance Show Error where
  show (ParseErr e) = show e
