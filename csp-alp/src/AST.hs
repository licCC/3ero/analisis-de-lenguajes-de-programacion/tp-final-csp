{-# LANGUAGE GADTs #-}

module AST where

---------------------------------------------------
-- Interactive mode

-- Comandos interactivos o de archivos
data Stmt i = Def String i
  deriving (Show)

instance Functor Stmt where
  fmap f (Def s i) = Def s (f i)

---------------------------------------------------
-- AST

data CSPTerm where
  Then :: String -> CSPTerm -> CSPTerm
  Choice :: String -> CSPTerm -> String -> CSPTerm -> CSPTerm
  IntChoice :: CSPTerm -> CSPTerm -> CSPTerm
  ExtChoice :: CSPTerm -> CSPTerm -> CSPTerm
  Sync :: CSPTerm -> CSPTerm -> CSPTerm
  Proc :: String -> CSPTerm
  Stop :: CSPTerm
  deriving Show

data Defexp where
  Decl :: String -> CSPTerm -> Defexp
  deriving Show

type Defs = [Defexp]
