module Analysis where

import AST ( Defexp(..), CSPTerm(..) )
import Data.List ( group, sort )

getAlphabet :: Defexp -> [String]
getAlphabet (Decl v t) = map head . group . sort $ getAlphabet' t
    where getAlphabet' (Then w u) = w : getAlphabet' u
          getAlphabet' (Choice w u x v) = [w] ++ getAlphabet' u ++ [x] ++ getAlphabet' v
          getAlphabet' (IntChoice u v) = getAlphabet' u ++ getAlphabet' v
          getAlphabet' (ExtChoice u v) = getAlphabet' u ++ getAlphabet' v
          getAlphabet' (Sync u v) = getAlphabet' u ++ getAlphabet' v
          getAlphabet' (Proc w) = [w]
          getAlphabet' Stop = []

firstevent :: CSPTerm -> [String]
firstevent (Then v _) = [v]
firstevent (Choice v _ s _) = [v, s]
firstevent (IntChoice t u) = 
    let t' = firstevent t
        u' = firstevent u
    in (t' ++ u')
firstevent (ExtChoice t u) =
    let t' = firstevent t
        u' = firstevent u
    in (t' ++ u')
firstevent (Sync t u) =
    let t' = firstevent t
        u' = firstevent u
    in (t' ++ u')
firstevent (Proc v) = [v]
firstevent Stop = []
