module Eval where

import Monads
    ( MonadCSP,
      getDeclState,
      getNDecls,
      printCSP,
      addDecl,
      removeDecl,
      addEvent,
      lookupDecl,
      choose,
      askevent )
import Global ( DeclState )
import AST ( CSPTerm(..), Defexp(Decl) )
import Analysis ( firstevent, getAlphabet )
import Control.Monad.State
    ( StateT, MonadState(put, get), MonadTrans(lift), evalStateT )
import Data.Maybe (fromMaybe)
import Distribution.Simple.Utils (safeHead, safeTail)

type Sync a m = StateT Int m a

fresh :: MonadCSP m => String -> Sync String m
fresh s = do
  i <- get
  put (i + 1)
  return ("_" ++ s ++ "_" ++ show i)

evalCSP :: MonadCSP m => m ()
evalCSP = do
  ds <- getDeclState
  let nDecls = length ds
  evalStateT (evalCSP' nDecls nDecls False) 0

evalCSP' :: MonadCSP m => Int -> Int -> Bool -> Sync () m
evalCSP' iter n b = do
  ds <- lift getDeclState
  case ds of
    [] -> return ()
    (Decl s t,_):_ -> do
      let ft = firstevent t
      progress <- if null ft
        then step s t -- es alguna forma de stop: (stop ec stop) por ejemplo
        else stepDecl' s t (head ft) -- hay cosas que computar
      if iter /= 0 || b
        then let i = mod (iter + 1) n
                 reset = iter == 0 && n > 1
                 lock = not reset && (b || progress)
             in evalCSP' i n lock
        else do
          lift $ printCSP "Existe una traza del programa que genera deadlock"
          return ()

stepDecl' :: MonadCSP m => String -> CSPTerm -> String -> Sync Bool m
stepDecl' s t ft = do
  state <- lift getDeclState
  let (syncr, lockr) = sync (safeTail state) ft
  if syncr && not lockr
    then do
      syncstep ft
      return True
    else if lockr
      then do
        lift $ next s t
        return False
      else step s t

sync :: [DeclState] -> String -> (Bool, Bool)
sync st ft = foldr sync' (False, False) st
  where sync' (Decl s t, alph) (syncr, lockr) =
          let synccondition = [ft] == firstevent t
          in (syncr || synccondition , lockr || (elem ft alph && not synccondition))

syncstep :: MonadCSP m => String -> Sync Bool m
syncstep s = do
  n <- lift getNDecls
  syncstep' (n + 1) s
  -- n + 1 para que continue desde el próximo proceso

syncstep' :: MonadCSP m => Int -> String -> Sync Bool m
syncstep' 0 _ = return True
syncstep' m s = do
  ld <- lift getDeclState
  case ld of
    [] -> return True
    ((Decl n t,_):_) -> do
      if [s] == firstevent t
      then step n t
      else do
        lift $ next n t
        return True
      syncstep' (m - 1) s

-- Evalua un paso de un término
step :: MonadCSP m => String -> CSPTerm -> Sync Bool m
step s t = step' s t >> return True

step' :: MonadCSP m => String -> CSPTerm -> Sync () m
step' s (Then e t) =
  lift $ stepprocess e s (Just t)
step' s (Choice e1 t e2 u) = do
  let t' = Then e1 t
      u' = Then e2 u
  choice <- lift $ extchoose s t' u'
  if choice
    then lift (next s t')
    else lift (next s u')
step' s (IntChoice t u) = do
  choice <- lift choose
  if choice
    then lift (next s t)
    else lift (next s u)
step' s (ExtChoice t u) = do
  let ft = firstevent t
      fu = firstevent u
      p = fromMaybe False ((==) <$> safeHead ft <*> safeHead fu)
  choice <- if p
    then lift choose
    else lift $ extchoose s t u
  if choice
    then lift (next s t)
    else lift (next s u)
step' s (Sync t u) = do
  tvar <- fresh s
  uvar <- fresh s
  let tdecl = Decl tvar t
      udecl = Decl uvar u
  lift removeDecl
  lift $ addDecl tdecl (getAlphabet tdecl)
  lift $ addDecl udecl (getAlphabet udecl)
step' s (Proc e) = do
  lift $ stepprocess e s Nothing
step' s Stop = do
  lift removeDecl
  lift $ addEvent (s ++ ":stop" ++ "\n")

extchoose :: MonadCSP m => String -> CSPTerm -> CSPTerm -> m Bool
extchoose s t u = do
  let t' = firstevent t
      t'' = unwords t'
      u' = firstevent u
      u'' = unwords u'
  choice <- askevent t'' u''
  case choice of
    Just b -> return b
    Nothing -> extchoose s t u

next :: MonadCSP m => String -> CSPTerm -> m ()
next s t = do
  removeDecl
  addDecl (Decl s t) (getAlphabet (Decl s t))

stepprocess :: MonadCSP m => String -> String -> Maybe CSPTerm -> m ()
stepprocess e s (Just t) = do
  announceDecl s e
  next s t
stepprocess e s Nothing = do
  res <- lookupDecl e
  case res of
    Just (Decl _ w) -> next s w
    Nothing -> removeDecl >> announceDecl s e

announceDecl :: MonadCSP m => String -> String -> m ()
announceDecl s e = addEvent (s ++ ":" ++ e ++ "\n")
