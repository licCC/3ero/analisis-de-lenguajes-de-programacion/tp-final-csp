{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use <$>" #-}
module Parser where

import AST ( CSPTerm(..), Defexp(..), Defs )
import Text.Parsec hiding (runP, parse)
import qualified Text.Parsec.Token as Tok
import Text.ParserCombinators.Parsec.Language
    ( LanguageDef,
      emptyDef,
      GenLanguageDef(identLetter, commentLine, reservedNames,
                     reservedOpNames) )
import qualified Text.Parsec.Expr as Ex
import Text.Parsec.Expr (Operator, Assoc)
import Control.Monad.Identity (Identity)

type P = Parsec String ()

-----------------------
-- Lexer
-----------------------

lexer :: Tok.TokenParser u
lexer = Tok.makeTokenParser langDef

langDef :: LanguageDef u
langDef = emptyDef {
         commentLine = "--",
         reservedNames = ["do"],
         reservedOpNames = ["->",":","=","|","||","ic", "ec","{","}"]
        }

whiteSpace :: P ()
whiteSpace = Tok.whiteSpace lexer

natural :: P Integer
natural = Tok.natural lexer

stringLiteral :: P String
stringLiteral = Tok.stringLiteral lexer

parens :: P a -> P a
parens = Tok.parens lexer

identifier :: P String
identifier = Tok.identifier lexer

reserved :: String -> P ()
reserved = Tok.reserved lexer

reservedOp :: String -> P ()
reservedOp = Tok.reservedOp lexer

tyIdentifier :: P String
tyIdentifier = Tok.lexeme lexer $ do
  c  <- upper
  cs <- many (identLetter langDef)
  return (c:cs)

-----------------------
-- Parsers
-----------------------

name :: P String
name = identifier

stopexp :: P CSPTerm
stopexp = do
    reserved "stop"
    return Stop

tm :: P CSPTerm
tm = stopexp
    <|> prefixop
    <|> parens expr

binary :: String -> (CSPTerm -> CSPTerm -> CSPTerm) ->
    Assoc -> Operator String () Identity CSPTerm
binary s f = Ex.Infix (reservedOp s >> return f)

table :: [[Operator String () Identity CSPTerm]]
table = [[binary "ic" IntChoice Ex.AssocLeft,
          binary "ec" ExtChoice Ex.AssocLeft,
          binary "||" Sync Ex.AssocLeft]]

expr :: P CSPTerm
expr = Ex.buildExpressionParser table tm

procexp :: String -> P CSPTerm
procexp n = do
    return (Proc n)

thenexp :: String -> P CSPTerm
thenexp v = reservedOp "->" >> Then v <$> expr

choiceexp :: String -> P CSPTerm
choiceexp v = do
    reservedOp "->"
    p <- expr
    reservedOp "|"
    w <- name
    reservedOp "->"
    q <- expr
    if v == w
        then fail "Choice debe tener prefijos distintos"
        else return (Choice v p w q)

prefixop :: P CSPTerm
prefixop = do
    v <- name
    try (choiceexp v) <|>
        try (thenexp v) <|>
        procexp v

decl :: P Defexp
decl = do
    v <- name
    reservedOp "="
    t <- expr
    return (Decl v t)

execexpr :: P [String]
execexpr = do
    reserved "do"
    reservedOp "{"
    vs <- many1 name
    reservedOp "}"
    return vs

exprordecl :: P (Either Defexp CSPTerm)
exprordecl = try (Left <$> decl) <|> Right <$> expr

program :: P (Defs, [String])
program = do
    ds <- many1 decl
    es <- execexpr
    return (ds, es)

runP :: P a -> String -> String -> Either ParseError a
runP p s filename = runParser (whiteSpace *> p <* eof) () filename s

parse :: String -> CSPTerm
parse s = case runP expr s "" of
    Right t -> t
    Left e -> error ("no parse: " ++ show s)
