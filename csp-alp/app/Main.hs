{-# LANGUAGE RecordWildCards #-}

module Main where

import AST
import Parser
import Eval ( evalCSP )
import Monads
import Global
import Errors
import Analysis

import System.Console.Haskeline ( defaultSettings, getInputLine, runInputT, InputT )
import System.Exit ( exitWith, ExitCode(ExitFailure) )
import Data.List (nub, isPrefixOf, intercalate )
import Control.Exception ( catch , IOException )
import System.IO ( hPrint, stderr, hPutStrLn )
import Control.Monad.Catch (MonadMask)
import Text.ParserCombinators.Parsec ( eof )
import Text.Parsec.Prim ( runParser )
import Data.Char ( isSpace )
import Control.Monad.Trans
import Control.Monad.State
import Control.Monad.Except
import Options.Applicative

iname, iprompt :: String
iname = "CSP"
iprompt = "CSP> "

-- | Parser de banderas
parseMode :: Parser (Mode,Bool)
parseMode = (,) <$>
  (flag Interactive Interactive (long "interactive" <> short 'i' <> help "Ejecutar en forma interactiva")
  <|> flag Eval Eval (long "eval" <> short 'e' <> help "Evaluar programa")
  ) <*> pure False

-- | Parser de opciones general, consiste de un modo y una lista de archivos a procesar
parseArgs :: Parser (Mode, Bool, [FilePath])
parseArgs = (\(a,b) c -> (a,b,c)) <$> parseMode <*> many (argument str (metavar "FILES..."))

main :: IO ()
main = execParser opts >>= go
  where
    opts = info (parseArgs <**> helper)
      ( fullDesc
     <> progDesc "Evaluador de CSP"
     <> header "Evaluador de CSP" )

    go :: (Mode, Bool, [FilePath]) -> IO ()
    go (Interactive,opt,files) =
              runOrFail (Conf Interactive) (runInputT defaultSettings (repl files))
    go (m,opt, files) =
              runOrFail (Conf m) $ mapM_ evalFile files

runOrFail :: Conf -> CSP a -> IO a
runOrFail c m = do
  r <- runCSP m c
  case r of
    Left err -> do
      liftIO $ hPrint stderr err
      exitWith (ExitFailure 1)
    Right v -> return v

repl :: (MonadCSP m, MonadMask m) => [FilePath] -> InputT m ()
repl args = do
  lift $ setInter True
  lift $ catchErrors $ mapM_ evalFile args
  s <- lift get
  when (inter s) $ liftIO $ putStrLn
    (  "Entorno interactivo para CSP.\n"
    ++ "Escriba :? para recibir ayuda.")
  loop
  where
    loop = do
      minput <- getInputLine iprompt
      case minput of
        Nothing -> return ()
        Just "" -> loop
        Just x -> do
          c <- liftIO $ interpretCommand x
          b <- lift $ catchErrors $ handleCommand c
          maybe loop (`when` loop) b

evalFile ::  MonadCSP m => FilePath -> m ()
evalFile f = do
  mode <- getMode
  i <- getInter
  setInter False
  when i $ printCSP ("Abriendo " ++ f ++ "...")
  case mode of
    Interactive -> do
      (decls, execdecls) <- loadFile f
      handleDecls decls execdecls
    _ -> return ()
  setInter i

------------------------------------------------------------------
-- Parser de comandos interactivos

data Command = E EvalForm
             | PPrint String
             | Reload
             | Browse
             | Quit
             | Help
             | Noop

data EvalForm = EvalInteractive String
              | EvalFile String

data InteractiveCommand = Cmd [String] String (String -> Command) String

interpretCommand :: String -> IO Command
interpretCommand x =
  if ":" `isPrefixOf` x
  then do
    let (cmd,t') = break isSpace x
        t = dropWhile isSpace t'
        matching = filter (\(Cmd cs _ _ _) -> any (isPrefixOf cmd) cs) commands
    case matching of
      [] -> do
        putStrLn ("Comando desconocido `" ++ cmd ++ "'. Escriba :? para recibir ayuda.")
        return Noop
      [Cmd _ _ f _] -> do
        return (f t)
      _ -> do
        putStrLn ("Comando ambigüo, podría ser " ++ intercalate ", "
          ([ head cs | Cmd cs _ _ _ <- matching ]) ++ ".")
        return Noop
  else
    return (E (EvalInteractive x))

commands :: [InteractiveCommand]
commands = [
  Cmd [":browse"] "" (const Browse) "Ver los nombres en scope (depuración)",
  Cmd [":load"] "<file>" (E . EvalFile) "Cargar un programa desde un archivo",
  Cmd [":print"] "<exp>" PPrint "Imprime un término y sus ASTs sin evaluarlo",
  Cmd [":reload"] "" (const Reload) "Vuelve a cargar el último archivo cargado",
  Cmd [":quit",":Q"] "" (const Quit) "Salir del intérprete",
  Cmd [":help",":?"] "" (const Help) "Mostrar esta lista de comandos"]

helpTxt :: [InteractiveCommand] -> String
helpTxt cs =
  "Lista de comandos:  Cualquier comando puede ser abreviado a :c donde\n" ++
  "c es el primer caracter del nombre completo.\n\n" ++
  "<expr> evaluar la expresión\n" ++
  unlines (map (\ (Cmd c a _ d) ->
    let ct = intercalate ", " (map (++ if null a then "" else " " ++ a) c)
    in ct ++ replicate ((24 - length ct) `max` 2) ' ' ++ d) cs)

handleCommand :: MonadCSP m => Command -> m Bool
handleCommand cmd = do
  s@GlEnv {..} <- get
  case cmd of
    Quit -> return False
    Noop -> return True
    Help -> printCSP (helpTxt commands) >> return True
    Browse -> do
      printCSP (unlines (reverse (nub (map printDeclState glb))))
      return True
    E c -> do
      case c of
        EvalInteractive e -> evalPhrase e
        EvalFile f -> evalFile f
      return True
    Reload -> eraseLastFileDecls >> (getLastFile >>= evalFile) >> return True
    PPrint e -> printPhrase e >> return True
  where printDeclState (Decl s _, alpha) =
          s ++ " - alfabeto: " ++ show alpha

printPhrase :: MonadCSP m => String -> m ()
printPhrase x = do
  x' <- parseIO "<interactive>" exprordecl x
  printCSP "CSPTerm:"
  printCSP (show x')

evalPhrase ::  MonadCSP m => String -> m ()
evalPhrase x = do
  e <- parseIO "<interactive>" exprordecl x
  case e of
    Left d@(Decl v t) -> handleDecl d 
    Right t -> handleDecl (Decl "" t)

handleDecls :: MonadCSP m => Defs -> [String] -> m ()
handleDecls ds es = do
  addDecls ds (map getAlphabet ds) es
  evalCSP

handleDecl ::  MonadCSP m => Defexp -> m ()
handleDecl d = do
  s <- getDeclState
  setDecls [d]
  evalCSP

loadFile :: MonadCSP m => FilePath -> m (Defs, [String])
loadFile f = do
  let filename = reverse(dropWhile isSpace (reverse f))
  x <- liftIO $ catch (readFile filename)
    (\e -> do let err = show (e :: IOException)
              hPutStrLn stderr ("No se pudo abrir el archivo " ++ filename ++ ": " ++ err)
              return "")
  setLastFile filename
  parseIO filename program x

parseIO :: MonadCSP m => String -> P a -> String -> m a
parseIO filename p x =
  case runP p x filename of
    Left e  -> throwError (ParseErr e)
    Right r -> return r
