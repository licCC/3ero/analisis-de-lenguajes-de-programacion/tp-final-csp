\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{hyperref}
\usepackage{amsmath}

\RequirePackage[a4paper,hmargin=1in, vmargin=1.4in,footskip=0.25in]{geometry}
\addtolength{\textwidth}{0.2cm}
\setlength{\parskip}{8pt}
\setlength{\parindent}{0.5cm}
\setlength{\headheight}{13.6pt}
\linespread{1.25}

\title{Informe - Análisis de Lenguajes de Programación}
\author{Delfina Martín}
\date{Diciembre 2024}

\begin{document}

\maketitle

\section{Descripción del proyecto}
CSP, \textit{Communicating Sequential Processes}, es un lenguaje diseñado por C.A.R.Hoare que se utiliza para especificar sistemas. El lenguaje presentado está fuertemente inspirado en CSP y en su modelo de concurrencia.

Un proceso en CSP es un término compuesto por cadenas de caracteres y operadores. Una vez presentada la gramática del lenguaje implementado, mostraré la semántica formal que se le dió en el proyecto.
A lo largo del informe me referiré al lenguaje implementado como CSP' mientras que reservo CSP para el lenguaje original de Hoare.

La gramática del lenguaje CSP' es

\begin{align*}
    t := \, &r \, \rightarrow \, t\\
   &|\,  r \, \rightarrow t \, | \, s \, \rightarrow \, t\\
   &|\,  t \, \text{ ic } \, t\\
   &|\,  t \, \text{ ec } \, t\\
   &|\,  t \, || \, t\\
   &|\,  stop\\
   &|\,  n
\end{align*}

donde $r, s$ son nombres de eventos, es decir, cadenas de caracteres y $n$ son nombres de procesos. Debido al último caso, CSP' soporta definiciones recursivas.

Un programa en CSP es un listado de procesos seguido de un listado de nombres de procesos a ejecutar. El Abstract Syntax Tree se puede ver en src/AST.hs y refleja la gramática del lenguaje.

\subsection{Alfabetos}
Cada proceso cuenta con un alfabeto que no es más que un listado de eventos que contiene. En la ejecución de un programa CSP' contar con el alfabeto es clave porque la presencia o ausencia de eventos en un proceso hace que la ejecución requiera de una sincronización o de un bloqueo.

\subsection{Modelo de evaluación}

Todos los procesos en CSP' corren de manera concurrente según un modelo que se describe a continuación.
Un programa

\begin{align*}
    a \, = \, b \, \rightarrow \, stop\\
    c \, =\,  d \, \rightarrow \, stop
\end{align*}

difiere de otro programa

\begin{align*}
    a \, = \, b \, \rightarrow \, stop \, | \, d \, \rightarrow \, stop
\end{align*}

solamente en los nombres con los que se imprimiran los eventos que formen parte de la traza de ejecución. Es decir, serán equivalentes observacionalmente a nivel de eventos (pero no a nivel de procesos pues los nombres no coincidirán).
Un ejemplo de esto se puede ver con los ejemplos opcion/dobleopcion.csp y opcion/dobleopcion2.csp

\subsubsection{Ausencia de bloqueos}
La forma más simple de evaluación de un programa en CSP' ocurre cuando el primer evento del siguiente proceso a ejecutar no se encuentra en el alfabeto de ningún otro proceso que se esté corriendo en paralelo.
En este caso se imprime en pantalla el primer evento de cada proceso que se evalúa/simula alternadamente. Por ejemplo, el test laberinto.csp muestra el caso de un programa con procesos de alfabetos disjuntos.

\begin{align*}
    laberinto \, = (right \, \rightarrow \, up \, \rightarrow \, stop) \, || \, (down \, \rightarrow \, left \, \rightarrow \, stop)
\end{align*}

Se crean dos procesos laberinto\_0 y laberinto\_1 y se agregan en ese orden al estado del programa.
Luego se ejecutan alternadamente:

\begin{itemize}
    \item se muestra en pantalla laberinto\_0:right, se remueve del estado y se agrega al final laberinto\_0 = up $\rightarrow$ stop
    \item se muestra en pantalla laberinto\_1:down, se remueve del estado y se agrega al final laberinto\_1 = left $\rightarrow$ stop
    \item se muestra en pantalla laberinto\_0:up, se remueve del estado y se agrega al final laberinto\_0 = stop
    \item se muestra en pantalla laberinto\_1:left, se remueve del estado y se agrega al final laberinto\_1 = stop
    \item se muestra en pantalla laberinto\_0:stop y se remueve del estado
    \item se muestra en pantalla laberinto\_1:stop y se remueve del estado
\end{itemize}

Para terminar con el estado vacío.

\subsubsection{Bloqueos y sincronización}
Una restricción de ejecución de procesos CSP' se da cuando se quiere ejecutar un evento de un proceso $A$ pero otro proceso $B$ del estado lo incluye en el alfabeto y no es el siguiente evento a ejecutar en $B$. En este caso la ejecución del proceso $A$ se encola al final de todos los procesos pendientes a la espera de la sincronización. Luego de posiblemente varias ejecuciones de procesos en el estado ocurre que un conjunto de procesos tiene como primer evento a ejecutar el mismo evento.

Cuando esto ocurre, todos los primeros eventos que coinciden se ejecutan y luego se continúa la ejecución desde el siguiente al último proceso ejecutado.

En el ejemplo concurrente/withlocks.csp la ejecución paso a paso es:

\begin{align*}
    x \, = \, a \, \rightarrow \, b \, \rightarrow \, stop\\
    y \, = \, c \, \rightarrow \, a \, \rightarrow \, stop
\end{align*}

\begin{itemize}
    \item y:c (pues x está bloqueado por el evento a)
    \item x:a (sincroniza con y)
    \item y:a
    \item y:stop (continúa y y termina)
    \item x:b
    \item x:stop
\end{itemize}

\subsubsection{Deadlocks}

Podría ocurrir eventualmente que exista una cadena de dependencias en la ejecución que evite que (todos) los procesos puedan avanzar. Este caso particular se da cuando existe una dependencia circular de lock entre los procesos que se ejecutan concurrentemente.
En concurrente/deadlock.csp se muestra un ciclo de 2 procesos en deadlock y en concurrente/deadlock2.csp se muestra un ejemplo de un ciclo de 3 procesos en abrazo mortal.

Luego en concurrente/deadlocktardio.csp y concurrente/deadlocktardio\_i.csp se muestran ejemplos de programas que entran en deadlock luego de ejecutar algunos eventos.

\subsection{Operadores CSP'}

A continuación se provee una descripción informal de los operadores de CSP'. En la sección de Decisiones de diseño se detalla su implementación.

\begin{itemize}
    \item $b \rightarrow t$ el operador Then ejecuta b y continua con la ejecución de t.
    \item $r \rightarrow t | s \rightarrow u$ el operador Choice es una versión de external choice donde se exponen los primeros eventos a ejecutar
    \item $t \text{ ic } u$ el operador IntChoice (internal choice) tira una moneda y en base al resultado continúa la ejecución con uno u otro de los procesos involucrados
    \item $t \text{ ec } u$ el operador ExtChoice (external choice) se comporta diferente de acuerdo a los primeros eventos de cada uno de los procesos
        \subitem si los eventos que anteceden a los procesos son el mismo, se comporta como un internal choice
        \subitem si los eventos que anteceden a los procesos son diferentes, pide al usuario que se indique por cuál camino seguir
    \item $t \, || \, u$ el operador Sync equivale a ejecutar los procesos $t$ y $u$ concurrentemente
    \item $n$ el operador Proc busca si existe en el listado de declaraciones globales algun proceso de nombre $n$
        \subitem si existe, agrega al estado la declaración con dicho nombre
        \subitem si no existe, asume que es un evento y lo muestra en pantalla en conjunto con el nombre del proceso que lo contiene
    \item $stop$ el operador Stop muestra stop en pantalla en conjunto con el nombre del proceso que lo contiene
\end{itemize}

\section{Instalación y Uso}

Para probar el funcionamiento de CSP' se debe

Compilar con \texttt{cabal build}, luego ejecutar con \texttt{cabal exec csp-alp-exe} y finalmente cargar el archivo deseado con el comando load. Por ejemplo: \texttt{:load test/concurrente/withlocks.csp}

\section{Decisiones de diseño relevantes y detalles de implementación}
El estado global que usa el programa se puede ver en Global.hs. Los campos relevantes son

\begin{itemize}
    \item decls: un listado de declaraciones globales
    \item glb: un listado de pares declaraciones globales a ejecutar - alfabetos de dichas declaraciones
    \item trace: el listado de eventos que se ejecutan y se muestran en pantalla
\end{itemize}

En lo que sigue, cuando hable de estado estaré hablando del listado glb salvo que indique lo contrario. De dicho listado depende la ejecución de un programa en CSP'.

\subsection{Mónada CSP}
La definición de la mónada CSP se puede ver en src/Monads.hs

\texttt{class (MonadIO m, MonadState GlEnv m, MonadError Error m, MonadReader Conf m) => MonadCSP m where}

que extiende de
\begin{itemize}
    \item MonadIO porque el internal choice requiere que se genere un número aleatorio
    \item GlEnv para llevar el estado global
    \item Error para poder mostrar mensajes de error
    \item MonadReader para leer de entrada estándar el modo de evaluación
\end{itemize}

\subsection{Análisis del evaluador}
La función principal de evaluación $evalCSP$ utiliza una función auxiliar $evalCSP'$ que recibe
\begin{itemize}
    \item Un iterador que se inicializa con el número de declaraciones globales
    \item El número de declaraciones globales
    \item Un booleano que indica si se progresó en la última vuelta de ejecución del estado que se inicializa en False
\end{itemize}

La función $evalCSP'$ verifica si el estado es una lista vacía. En el caso de serlo, retorna. En otro caso, identifica si hay cosas que computar.
En el caso que haya progreso en la última vuelta, se continúa con otra vuelta de evaluación. En caso contrario, se indica que ocurrió un deadlock.
Una vuelta de evaluación en $stepDecl'$ consiste en filtrar las declaraciones que contengan en el alfabeto el evento que se está ejecutando.

\begin{itemize}
    \item Si el evento está en una posición diferente a primera en algún evento, el proceso se bloquea. Es decir, se continúa ejecutando el siguiente proceso.
    \item Si el evento está en otros procesos en primera posición entonces se sincroniza con todos ellos.
    \item En caso de que no esté en ningún otro proceso, se ejecuta el evento.
\end{itemize}

Finalmente, la ejecución de un solo proceso está implementada en la función $step$ en donde la implementación de cada caso refleja la descripción informal de las operaciones vista más arriba.
Es relevante destacar las funciones de la mónada CSP $choose$ y $extchoose$ que tiran una moneda y le preguntan al usuario por cuál proceso seguir respectivamente.

El entero en el tipo \texttt{type Sync a m = StateT Int m a} se utiliza para generar varibles frescas para nombrar a los procesos que se generan producto del uso del operador Sync.

\section{Trabajos en los que está inspirado}

\begin{itemize}
    \item trabajos prácticos durante el cursado de la materia Análisis de Lenguajes de Programación.
    \item compilador inicial de la materia Compiladores
\end{itemize}

\section{Trabajo Futuro}

Algunas de las posibles mejoras para el proyecto consisten en:
\begin{itemize}
    \item Utilizar la implementación de lista circular provista en \url{https://hackage.haskell.org/package/data-clist-0.1.2.3/docs/Data-CircularList.html}
    para hacer más eficiente el proceso de agregar nuevas declaraciones a la cola estado.
    \item Mejorar el parser para que muestre con mejor detalle los errores en caso de fallar.
\end{itemize}

\end{document}
